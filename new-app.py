from flask import Flask, request, Response, g, render_template
import jsonpickle
import numpy as np
from neuralcoref import Coref
from flask import jsonify
from es_utils import es_search
from es_utils import es_import
import spacy 
nlp = spacy.load("en")

# Initialize the Flask application
app = Flask(__name__)
coref = Coref()


@app.route('/webapp/test',methods=['POST'])
def main():
	document = request.json
	print(document['content'])
	result = run_test()
	print(result)
	render_template(index.html,search = result.words, document = document.words)
	return jsonify(result=result)


#----------------------------------------#

# route http posts to this method
@app.route('/api/test', methods=['POST'])
def test():
    result = {}
    document = request.json
    print(document['content'])
    documents = nlp(document['content'])
    print(type(documents.sents))
    sentences = [document for document in documents.sents]
    print(sentences)
    context = str(sentences[0])
    utterances = str(sentences[1:])
    clusters = coref.one_shot_coref(utterances=utterances, context=context)
    resolved_utterance_text = coref.get_resolved_utterances()
    #entities = []
    entities = {'label': [], 'txt':[]}
    for ent in documents.ents:
        #entities.append((ent.text, ent.label_))
        entities['label'].append( ent.label_)
        entities['txt'].append( ent.text)
    #result['coreference'] = resolved_utterance_text
    result['coref'] = [i.rstrip().lstrip() for i in resolved_utterance_text[0].replace('[', '').replace(']', '').split(',')]
    result['entities'] = entities
    es_import(result['coref'], result['entities']['txt'], result['entities']['label'])
    return jsonify(result=result)    

#@app.route('/upload')
#def upload():
#    return render_template('upload.html')
       
#@app.route('/uploader', methods = ['GET', 'POST'])
#def uploader():
#    if request.method == 'POST':
#        f = request.files['file']
#        f.save(secure_filename(f.filename))
#        return 'file uploaded successfully'
# start flask app

app.run(host="0.0.0.0", port=5601)
